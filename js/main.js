const API_KEY = 'a459a318025fc2eb5364836b0f32e3b8';

$(document).ready(function () {
    // Form search
    $('#searchForm').submit(function (e) {
        e.preventDefault();
        sessionStorage.setItem('movieSearch', $('#searchText').val());
        sessionStorage.removeItem('movieType');
        window.location = 'index.html';
    });
    // List types
    $('#movie_types > li > a').click(function () {
        sessionStorage.setItem('movieType', $(this)[0].id);
        sessionStorage.removeItem('movieSearch');
        window.location = 'index.html';
    });
});

function getMovies(page = 1) {
    let link = 'https://api.themoviedb.org/3/';
    // Search or type
    let search = sessionStorage.getItem('movieSearch') || null;
    let type = sessionStorage.getItem('movieType') || 'popular';
    if (search) {
        link += 'search/movie' + '?api_key=' + API_KEY + '&query=' + search;
        $('#page_title').html('Searching: ' + search);
    } else {
        link += 'movie/' + type + '?api_key=' + API_KEY;
        $('#page_title').html(type.replace(/_/g, ' ') + ' Movies');
    }
    link += '&page=' + page;
    // Get movies
    $.get(link, function (data) {
        let movies = data.results;
        let output = '';
        $.each(movies, function (index, movie) {
            movie.overview = movie.overview.substr(0,200) + '...';
            if (movie.poster_path) {
                movie.poster_path = 'https://image.tmdb.org/t/p/w500/' + movie.poster_path;
            } else {
                movie.poster_path = 'http://placehold.it/500x750';
            }
            output += `<div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <img class="img-responsive fit-image" src="${movie.poster_path}" alt=""/>
                            </div>
                            <div class="col-md-6">
                                <h3>
                                    <a href="#" onclick="movieSelected(${movie.id})">${movie.title}</a>
                                </h3>
                                <p>${movie.release_date}</p>
                                <hr/>
                                <p>${movie.overview}</p>
                                <hr/>
                                <a href="#" class="btn btn-primary" onclick="movieSelected(${movie.id})">Read More</a>
                            </div>
                        </div>
                       </div>`;
        });
        $('#movies').html(output);
        // Pagination
        let prev_page = $('#prev_page');
        let next_page = $('#next_page');
        prev_page.one('click', function () {
            getMovies(page - 1);
        });
        next_page.one('click', function () {
            getMovies(page + 1);
        });
        if(page <= 1) {
            prev_page.hide();
        } else {
            prev_page.show();
        }
        if(page > data.total_pages - 1) {
            next_page.hide();
        } else {
            next_page.show();
        }
    });
}

function movieSelected(id) {
    sessionStorage.setItem('movieId', id);
    window.location = 'movie.html';
    return false;
}

function getMovie() {
    let movieId = sessionStorage.getItem('movieId');
    $.get('https://api.themoviedb.org/3/movie/' + movieId + '?api_key=' + API_KEY, function (movie) {
        // Image
        if(movie.poster_path)
            $('#movie_image').attr('src', 'https://image.tmdb.org/t/p/w500/' + movie.poster_path);
        // Title
        $('#movie_title').html(movie.title);
        // Votes
        $('#movie_vote').html(movie.vote_average * 10 + '%');
        // Date
        $('#movie_date').html(movie.release_date);
        // Genres
        let genre_array = [];
        $(movie.genres).each(function (index, value) {
            genre_array.push(value.name);
        });
        $('#movie_genres').html(genre_array.join(', '));
        // Overview
        $('#movie_overview').html(movie.overview);
        // Cast
        $.get('https://api.themoviedb.org/3/movie/' + movieId + '/credits?api_key=' + API_KEY, function (credits) {
            let cast = "";
            $(credits.cast).each(function (index, actor) {
                cast += `<tr>
                            <th>${actor.order + 1}</th>
                            <td>${actor.name}</td>
                            <td>${actor.character}</td>
                        </tr>`;
            });
            $('#movie_cast tbody').html(cast);
        });
    });
}